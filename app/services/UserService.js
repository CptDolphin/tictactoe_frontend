'use strict';

angular.module('myApp.userService', ['ngRoute'])
    .service('UserService', ['$http', '$rootScope', 'AuthService', function ($http, $rootScope, AuthService) {
        var URL = "http://localhost:8080";

        this.getAllUsers = getAllUsers;

        function getAllUsers() {
            $http.get(URL + '/user/list').then(
                function (odpowiedz) {
                    $rootScope.usersL = [];
                    for (var index in odpowiedz.data) {
                        var user = odpowiedz.data[index];
                        $rootScope.usersL.push(user);
                    }
                    console.log(odpowiedz.data);
                }, function (odpowiedzBlad) {
                    console.log(odpowiedzBlad);
                });
        }

        // function findById(id) {
        //     for (var i = 0; i < $rootScope.tasks.length; i++) {
        //         if ($rootScope.tasks[i] === id) {
        //             return $rootScope.tasks[i];
        //         }
        //     }
        // }

    }]);

