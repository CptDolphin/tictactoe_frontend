'use strict';

angular.module('myApp.roomService', ['ngRoute'])
    .service('RoomService', ['$http', '$rootScope', function ($http, $rootScope) {
        var URL = "http://localhost:8080";
        this.getAllRooms = getAllRooms;

        function getAllRooms() {
            $http.get(URL + '/room/getAll')
                .then(function (odpowiedz) {
                    $rootScope.rooms = [];
                    for (var index in odpowiedz.data){
                        $rootScope.rooms.push(odpowiedz.data[index]);
                    }
                    console.log(odpowiedz.data);
                }, function (odpowiedzKiedyBlad) {
                    console.log(odpowiedzKiedyBlad);
                });
        }

    }]);