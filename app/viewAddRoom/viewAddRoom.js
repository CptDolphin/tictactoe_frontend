'use strict';

angular.module('myApp.viewAddRoom', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewAddRoom', {
            templateUrl: 'viewAddRoom/viewAddRoom.html',
            controller: 'ViewAddRoomCtrl'
        });
    }])

    .controller('ViewAddRoomCtrl', ['$http', function ($http) {
        var self = this;
        var URL = "http://localhost:8080";
        this.listaPokojow = [];

        this.roomCreateDto = {
            'name': ''
        };

        this.dodajPokoj = function () {
            $http.post(URL + '/room/add', self.roomCreateDto)
                .then(
                    function (odpowiedz) {
                        console.log(odpowiedz);
                    },
                    function (odpowiedzNaBlad) {
                        console.log(odpowiedzNaBlad);
                    });
        };
    }]);