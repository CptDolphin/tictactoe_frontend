'use strict';

angular.module('myApp.viewSearchinRoom', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/viewSearchinRoom', {
    templateUrl: 'viewSearchinRoom/viewSearchinRoom.html',
    controller: 'ViewSearchinRoomCtrl'
  });
}])

.controller('ViewSearchinRoomCtrl', ['$http', 'RoomService', function ($http, RoomService) {
    var URL = "http://localhost:8080";
    var self = this;
    this.zmienna = {};
    this.szukanyPokoj = 1;

    RoomService.getAllRooms();

    this.pobierzDane = function () {
        $http.get(URL + "/room/get/" + self.szukanyPokoj)
            .then(
                function (odpowiedz) { // ta funkcja wykona sie jesli bedzie sukces
                    console.log(odpowiedz);
                    // system.out.println
                    self.zmienna = odpowiedz.data;
                },
                function (odpowiedzWPrzypadkBledu) { // ta funkcja jesli bedzie blad

                });
    };

}]);