'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'myApp.viewAddRoom',
    'myApp.viewLogin',
    'myApp.viewRegister',
    'myApp.viewSearchinRoom',
    'myApp.viewConnectedUsers',
    'myApp.roomService',
    'myApp.authService',
    'ui.router',
    'myApp.version'
])
    .config(['$locationProvider', '$routeProvider', '$stateProvider',
        function ($locationProvider, $routeProvider, $stateProvider) {
            $locationProvider.hashPrefix('!');

            $routeProvider.otherwise({redirectTo: '/viewLogin'});

            $stateProvider
                .state('viewLogin', {
                    url: '/viewLogin',
                    templateUrl: 'viewLogin/viewLogin.html',
                    controller: 'ViewLoginCtrl'
                })
                .state('viewRegister', {
                    url: '/viewRegister',
                    templateUrl: 'viewRegister/viewRegister.html',
                    controller: 'ViewRegisterCtrl'
                });

        }]).run(function (AuthService, $http, $window, $rootScope) {
    //zawsze wykona sie przed odswierzeniem ramki

    // $rootScope.logout = function () {
    //     $window.sessionStorage.removeItem('token');
    //     $window.sessionStorage.removeItem('user_id');
    //     AuthService.loggedInUser.id = '';
    // };

    if (AuthService.loggedInUser === '') {
        //jestesmy niezalogowani
        var token = sessionStorage.getItem('token');
        var user_id = sessionStorage.getItem('user_id');

        if (!token || !user_id) {
            // if (token == null || token === undefined || user_id == null || user_id === undefined){

            // to znaczy ze nie mamy danych logowania w sesjii
            console.log(token)
            location = "#!/viewLogin";
        } else {
            console.log(token)
            $http.defaults.headers.common.Authorization = 'Bearer ' + token;
            //one bloody .id missing *_*
            AuthService.loggedInUser.id = user_id;
        }
    }

}).run(function ($rootScope, AuthService, $location) {
    $rootScope.$on('$routeChangeStart', function ($event, next, current) {
        if (next.originalPath !== '/viewLogin' && next.originalPath !== '/viewRegister') {
            console.log(AuthService.loggedInUser.id);
            if (AuthService.loggedInUser.id === '') {
                $location.path('/viewLogin');
            }
        }
    });
});

// $rootScope.logout = function () {
//     $window.sessionStorage.removeItem('token');
//     $window.sessionStorage.removeItem('user_id');
//     AuthService.loggedInUser = '';
// };