'use strict';

angular.module('myApp.viewConnectedUsers', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewConnectedUsers', {
            templateUrl: 'viewConnectedUsers/viewConnectedUsers.html',
            controller: 'ViewConnectedUsers'
        });
    }])

    .controller('ViewConnectedUsers', ['$http', '$routeParams', '$window', 'AuthService', function ($http, $routeParams, $window, AuthService) {
        var URL = "http://localhost:8080";
        var self = this;
        this.changeUser = $routeParams.changeUser;
        AuthService.loggedInUser = this.changeUser;

        console.log(AuthService.loggedInUser);
        $window.location = "#!/view1";
    }]);