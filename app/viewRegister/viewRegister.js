'use strict';

angular.module('myApp.viewRegister', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/viewRegister', {
    templateUrl: 'viewRegister/viewRegister.html',
    controller: 'ViewRegisterCtrl'
  });
}])

.controller('ViewRegisterCtrl', ['$http', '$routeParams' , 'AuthService', function ($http, $routeParams, AuthService) {
    var URL = "http://localhost:8080";


    this.sendRegisterForm = function () {
        $http.post(URL + '/auth/register', {
            login:this.login,
            name:this.name,
            password:this.password
        }).then(
                function (odpowiedz) {
                    console.log(odpowiedz);
                },
                function (odpowiedzNaBlad) {
                    console.log(odpowiedzNaBlad);
                }
            );
    };
}]);