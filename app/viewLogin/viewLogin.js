'use strict';

angular.module('myApp.viewLogin', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewLogin', {
            templateUrl: 'viewLogin/viewLogin.html',
            controller: 'ViewLoginCtrl'
        });
    }])
    .controller('ViewLoginCtrl', ['$http', '$routeParams', 'AuthService',
        function ($http, $routeParams, AuthService) {
            var URL = "http://localhost:8080";


            this.sendLoginForm = function () {
                // console.log(this.login);
                // console.log(this.password);
                $http.post(URL + '/auth/authenticate', {
                    login:this.login,
                    password:this.password
                })
                    .then(
                        function (odpowiedz) {
                            //jeśli się uda
                            console.log(odpowiedz);

                            var token = odpowiedz.data.token;
                            var user_id = odpowiedz.data.id;

                            $http.defaults.headers.common.Authorization = 'Bearer ' + token;

                            sessionStorage.setItem('token', token);
                            sessionStorage.setItem('user_id', user_id);
                            console.log(token);
                            console.log(user_id);

                            AuthService.loggedInUser.id = user_id;

                        },
                        function (odpowiedzKiedyBlad) {
                            // nie udało się
                            console.log(odpowiedzKiedyBlad);
                        });
            }
        }]);
